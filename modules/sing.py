# sing.py

import random

# Metadata

NAME    = 'sing'
ENABLE  = True
TYPE    = 'command'
PATTERN = '^!sing'
USAGE   = '''Usage: !sing
Bobbit will sing to the designated user some of its favorite song lines.
Example:
    > !sing
    Never gonna give you up! Never gonna let you down!
'''

# Constants

RESPONSES = (
        'Never gonna give you up! Never gonna let you down!',
        'Every move you make, every step you take, Ill be watching you',
        'Hey, I just met you and this is crazy, but heres my number, so call me maybe',
        'Let it go. LET IT GOOOOOOOOOO',
        'HELLO, its me',
        'Hi! My name is (what?), my name is (who?), my name is B-B-B-B-B-Bobbit',
        'All I want for Christmas is yooooouuuuuuu, you baaaby!',
        'If youre lost you can look and you will find me, time after time',
        'Look what you made me do',
        'It cannot wait, Im yoooouuuurrrsss do do do do dodododododo do do do do do do do',
        'When your legs dont work like they used to before. (Wish I could relate)',
        'a-b-c-d-e-f-g-h-i-j-k-lmno-p-q-r-s----t-u-v----w-x----y and z now I know my abcs, next time wont you sing with me',
        'S-I-M-P, Squirrels in my Pants... How can I qualify for government graaaants... S-I-M-P, Squirrels in my Pants...',
        'I hate you, I love you, I hate that I love you, dont want to but I cant put no body else above you',
        'If you down get up oh oh... when you down get up eh eh... Tsamina mina zangalewa... This time for Africa',
        '**Darude Sandstorm** Duuuuuuuuuuuuuuuuuuuuuuun Dun dun dun dun dun dun dun dun dun dun dun dundun dun dundundun dun dun dun dun dun dun dundun dundun BOOM Dundun dundun dundun BEEP Dun dun dun dun dun dun dun BEEP BEEP BEEP BEEP',
        'Rockabye baby on a treetop, when the wind blows the cradle will rock',
        'STOP... wait a minute, fill my cup put some *soda* in it',
        'In the name of LOOOvveeeee name of LOOOVvveEEE In the name of LOooOOVVeee name of LOOOOooovvee',
)

# Command

def command(bot, nick, message, channel, question=None):
    response = random.choice(RESPONSES)
    bot.send_response(response, nick, channel)

# Register

def register(bot):
    return (
        (PATTERN, command),
    )

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
